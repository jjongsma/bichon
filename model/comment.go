// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package model

import (
	"time"
)

type Comment struct {
	Author      User      `json:"author"`
	CreatedAt   time.Time `json:"createdAt"`
	UpdatedAt   time.Time `json:"updatedt"`
	Description string    `json:"description"`
	System      bool      `json:"system"`

	Context *CommentContext `json:"context"`
}

type CommentThread struct {
	ID         string    `json:"id"`
	Individual bool      `json:"individual"`
	Comments   []Comment `json:"comments"`
}

type CommentContext struct {
	BaseHash  string `json:"baseHash"`
	StartHash string `json:"startHash"`
	HeadHash  string `json:"headHash"`
	NewFile   string `json:"newFile"`
	NewLine   uint   `json:"newLine"`
	OldFile   string `json:"oldFile"`
	OldLine   uint   `json:"oldLine"`
}
